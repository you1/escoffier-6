---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---




\thispagestyle{empty}



<div class="recette">

\normalsize

* **五 島　学**

\small

1966年東京生まれ。東京都立大学大学院博士課程単位取得満期退学（仏文学専
攻）。宇都宮大学、明治大学等非常勤講師を経て2005年より在野でフランス食
文化史研究に携わりつつレストラン向けにヨーロッパ品種の野菜を生産。2011
年〜2014年、柴田書店「月刊 専門料理」連載「エスコフィエを読む」におい
て訳と注釈を担当。
\columnbreak
\normalsize
* **河井 健司**

\small

1973年東京生まれ、高知県育ち。「レストラン・サマーシュ」など、 日本で
10年修行後28歳で渡仏。パリ三ツ星の名店「リュカ・カルトン」 に入店。フ
ランス料理界重鎮アラン・サンドランス氏に師事。各部門 長の後、同系列の
会員制レストラン「ル・セルクル」の料理長 を務めた。その後二ツ星「サン
ドランス」に於いて副料理長として勤務。 5年半のフランス滞在を終え帰国。
六本木オーシザーブル料理長を経て、 現在に至る。

\normalsize



</div><!--endRecette-->

\vfil

\vfil

\vfil


<div class="flushright">


\Huge エスコフィエ『フランス料理の手引き』全注解

</div><!--endFlushright-->

------------------------------------------------------

<div class="flushright">

\large 2019年8月31日初版1刷発行

\vspace{2zw}

\vspace{2ex}

\normalsize

オーギュスト・エスコフィエ 原著

\normalsize

Autugste Escoffier, *Le guie culinaire*, Flammarion, $4^e$ éd., 1921.


\Large

編（訳、注釈）・\LuaLaTeX \hspace{0zw}組版……五島\hspace{.25zw}学
編（訳、注釈）……河井\hspace{.25zw}健司

\normalsize

協力（下訳、校閲、校正）…………江畑\hspace{.25zw}雄一、河井\hspace{.25zw}健司、五島\hspace{.25zw}充代、善搭\hspace{.25zw}一幸

春野\hspace{.25zw}裕征、山下\hspace{.25zw}拓也、山本\hspace{.25zw}学


制作マネジメント……オフィスSNOW

表紙・扉・目次・奥付デザイン、装丁……津嶋デザイン事務所（津嶋佐代子）

発行者……□□ □□

</div><!--endFlushright-->



----------------------------------------------------------


<div class="flushright">

\Large\setstretch{0.8}

発行所……\Large 株式会社 \LARGE 旭屋出版

\normalsize

〒107-0052 東京都港区赤坂1-7-19

キャピタル赤坂ビル8 F

電話……03-3560-9065（販売）

03-2560-9066（編集）

FAX……03-3560-9071（販売）

http://www.asahiya-jp.com

郵便振替 00150-1-19572


\Large 印刷・製本……□□□□□□

</div><!--endFlushright-->

\setstretch{0.8}

© 2019 五島 学、オフィスSNOW、旭屋出版

無断転載、翻訳、複写、データ配信等を禁ず。

ISBN ○○○-○-○○○-○○○○○-○

Printed in Japan
