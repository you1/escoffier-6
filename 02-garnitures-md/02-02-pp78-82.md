---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}-->





## 温製ガルニチュール用アパレイユ[^0202_2]など {#series-des-appareils-et-preparations-diverses-pour-garnitures-chaudes}

<!--<div class="frsecenv">Série des Appareils et Préparations diverses pour Garnitures chaudes</div>-->

\index{garniture@garniture!appareils chaudes@appareils et préparations diverses pour ---s chaudes}
\index{appareil@appareil!garnitures chaudes@--- et préparations diverses pour garnitures chaudes}
\index{かるにちゆる@ガルニチュール!あはれいゆおんせい@温製---のためのアパレイユなど}
\index{あはれいゆ@アパレイユ!おんせいかるにちゆる@温製ガルニチュールのための---など}

[^0202_2]: 巻末付録、基本用語集、[アパレイユ](#appareil-gls)の項を参照。


<div class="recette">


#### クロメスキとクロケット[^0202_3]の[アパレイユ\*](#appareil) {#appareils-a-cromesquis-et-a-croquettes}

<div class="frsubenv">Appareils à Cromesquis et à Croquettes</div>


\index{appareil@appareil!cromesquis croquettes@---s à cromesquis et à croquettes}
\index{cromesqui@cromesqui!appareil@appareils à --- et à croquettes}
\index{croquette@croquette!appareil@appareils à cromesquis et à ---}
\index{あはれいゆ@アパレイユ!くろめすきとくろけつと@クロケットとクロメスキの---}
\index{くろめすき@クロメスキ!あはれいゆ@---とクロケットのアパレイユ}
\index{くろけつと@クロケット!あはれいゆ@クロメスキと---のアパレイユ}

⇒　[温製オードブル](#serie-des-hors-d-oeuvre-chauds)参照。

[^0202_3]: クロケットは日本のコロッケの原型となったもので、細かく切っ
た素材をじゃがいものピュレや[ベシャメルソース](#sauce-bechamel)であえ
て円盤または円筒形に整形してパン粉衣を付けて揚げたもの。クロメスキは正
六面体（サイコロ形）にすることが多く、コロッケとアパレイユが共通のため、
形状が違うだけでクロケットのバリエーションという見方もあるが、ポーラン
ド語のkromesk（薄く切ったもの）が語源とされる。





\atoaki{}

#### ポム・ドフィーヌ、デュシェス、マルキーズ[^0202_4]のアパレイユ {#appareils-a-pomme-dauphine-duchesse-marquise}

<div class="frsubenv">Appareils à pomme Dauphine, Duchesse et Marquise</div>


\index{appareil@appareil!pomme dauphine@---s à pomme Dauphine, Duchesse et Marquise}
\index{dauphin@dauphin(e)!appareil@appareil à pomme ---e}
\index{duc@duc / duchesse!appareil@appareil à pomme duchesse}
\index{marquis@marquis(s)!appareil@appareil à pomme ---e}
\index{あはれいゆ@アパレイユ!ほむ とふいぬ てゆしえす まるきす@ポム…ドフィーヌ、デュシェス、マルキーズの---}
\index{とふいぬ@ドフィーヌ!あはれいゆ@アパレイユ!ほむ とふいぬ てゆしえす まるきす あはれいゆ@じゃがいも・---、デュシェス、マルキーズのアパレイユ}
\index{てゆしえす@デュシェス!あはれいゆ@アパレイユ!ほむ とふいゆぬ てゆしえす まるきす@ポム・ドフィーヌ、---、マルキーズのアパレイユ}
\index{まるきす@マルキーズ!あはれいゆ@アパレイユ!ほむ とふいゆぬ てゆしえす まるきす@ポム・ドフィーヌ、デュシェス、---のアパレイユ}

⇒　野菜料理、[じゃがいも](#pommes-de-terre)参照。

[^0202_4]: dauphin（王太子）、dauphine（王太子妃）、duc（公爵）、
    duchesse（公爵夫人）、mariquis（侯爵）、marquise（侯爵夫人）。いず
    れも王家、貴族の位階（爵位）を表わす語だが、特に理由もなく料理名に
    付けられることが非常に多い。





\atoaki{}

#### アパレイユ・マントノン[^0202_5] {#appareil-maintenon}

<div class="frsubenv">Appareil Maintenon</div>


\index{appareil@appareil!maintenon@--- Maintenon}
\index{maintenon@Maintenon!appareil@appareil ---}
\index{あはれいゆ@アパレイユ!まんとのん@---・マントノン}
\index{まんとのん@マントノン!あはれいゆ@アパレイユ・---}


（[羊のコトレット・マントノン](#cotelettes-maintenon)用）

[ベシャメルソース](#sauce-bechamel)4 dLと[スビーズ](#sauce-soubise)1
dLを半量になるまで煮詰める。

卵黄3個を加えてとろみを付ける。あらかじめマッシュルーム100 gを[エマンセ\*](#emincer-gls)に
してバターで[エチュヴェ\*](#etuver-gls)しておいたたものを加える。


[^0202_5]: マントノン夫人（出生名フランソワーズ・ドビニェ 1635〜1719）。は
    じめはマントノン侯爵夫人としてルイ14世とモンテスパン夫人の間に生ま
    れた子どもたちの非公式な教育係となり、モンテスパン夫人の死後、ルイ
    14世と結婚した。彼女の名を冠した料理はここで言及されている[羊のコ
    トレット　マントノン](#cotelettes-maintenon)の他、卵料理、菓子など
    にある。「羊のコトレット　マントノン」は彼女自身が考案したとも、ル
    イ14世付の料理人の考案ともいわれているが、いずれも憶測の域を出ない。
    なお、côtelette（コトレット）とは仔牛、羊の背肉を骨付きで肋骨1本ず
    つに切り分けたもの。日本語では、仔羊の場合ラムチョップと呼ばれるこ
    とも多い。






\atoaki{}

#### アパレイユ・モングラ[^0202_6] {#appareil-a-la-montglas}

<div class="frsubenv">Appareil à la Montglas</div>


\index{appareil@appareil!montglas@--- à l Montglas}
\index{montglas@Montglas!appareil@appareil à la ---}
\index{あはれいゆ@アパレイユ!もんくら@---・モングラ}
\index{もんくら@モングラ!あはれいゆ@アパレイユ・---}



（[羊のコトレット・モングラ](#cotelettes-monglas)用など）

[ロングエカルラット\*](#langue-ecarlate-gls)150 g、フォワグラ150 g、
茹でたマッシュルーム100 g、トリュフ100 gを通常より太めで短かい[ジュリ
エンヌ\*](#julienne-gls)にする。

これらを、マデイラ酒風味の充分に煮詰めた[ソース・ドゥミグラ
ス](#sauce-demi-glace)2 $\frac{1}{2}$ dLであえ、バターを塗った平皿に広げて
使うまでそのまま冷ます。

[^0202_6]: Salpicon à la Monglas（[サルピコン](#salpicons-divers)アラ
    モングラ）とも呼ばれものと同じ。<!--[サルピコ
    ン](#salpicons-divers)は5 mm角くらいの小さなさいの目に切ったものの
    こと。-->この本文で示されている以外の用途としては、ブシェ（パイ生
    地で作ったケースに詰め物をしたもの。本書ではオードブルに分類されて
    いる）やタルトレット（小さなタルト）の\kenten{アパレイユ}にする。
    カレーム『19世紀フランス料理』にはプロフィットロール（小さな丸いパ
    ンの中身を刳り貫いたもの）にフォワグラとロングエカルラットとマッシュ
    ルームのサルピコンを詰めた「プロフィットロールのポタージュ　モング
    ラ」が掲載されている(t.1, p.180)。また1806年刊のヴィアール『帝国料
    理の本』にはローストしたペルドローの胸肉とマッシュルーム、トリュフ
    のサルピコンをソース・エスパニョルなどであえた「ペルドロー・モング
    ラ」が掲載されている(pp.265-266)。<!--それ以前の主な料理書にこの料
    理名は見当たらないが、-->17世紀のモングラ侯爵 François Clermont
    Marquis de Montglas （生年不詳〜1675）の名を冠したものらしい。







\atoaki{}

#### プロヴァンス風アパレイユ[^0202_8] {#appareil-provencal}

<div class="frsubenv">Appareil à la Provençale</div>

\index{appareil@appareil!provencale@--- à la Provençale}
\index{provençal@provençale(e)!appareil@appareil à la ---e}
\index{あはれいゆ@アパレイユ!ふろうあんすふう@プロヴァンス風---}
\index{ふろうあんすふう@プロヴァンス風!あはれいゆ@---アパレイユ}


（[羊のコトレット・プロヴァンス風](#cotelettes-provencale)用）

[ソース・スビーズ](#sauce-soubise)5 dLを充分に固くなるまで煮詰める。潰
したにんにく1片を加え、卵黄3個を加えてとろみを付ける。

[^0202_8]: [アパレイユ・マントノン](#appareil-maintenon)からここまで3種の
    アパレイユはいずれも、羊の[コトレット\*](#cotelette-gls)（ラムチョップ）の片面だけを焼
    いて、その表面をよく拭い、まだ焼いていない面を下にして、焼いた側の
    面にこれらのアパレイユを塗る、あるいは盛り上げてからオーブンに入れ
    るという同工異曲とも言うべき仕立てに用いられる。ここで、アパレイン・
    マントノンとこのプロヴァンス風アパレイユの「用途」の部分の原文には
    動詞farcirあるいはその過去分詞farci(es)が用いられているのはとても
    興味深いと言えよう。farcirを日本語の「詰め物をする」と等価と考えて
    はうまく理解できないケースのひとつで、日本語としてはこの場合「盛る」
    のほうがむしろ適切だろう。




\atoaki{}

#### ファルスで作る縁飾り {#bordures-en-farce}

<div class="frsubenv">Bordures en farce</div>

\index{bordure@bordure!farce@--- en farce}
\index{farce@farce!bordures@bordures en ---}
\index{ふあるす@ファルス!ふちかざり@---で作る縁飾り}
\index{ほるてゆる@ボルデュール ⇒ 縁飾り!ふぁるす@ファルスで作る縁飾り}
\index{ふちかさり@縁飾り!ふあるす@ファルスで作る---}

この縁飾りは、飾り付ける料理の素材とおなじ材料を中心にしたファルス[^0202_12]を
使う。縁飾り用の型[^0202_9]はプレーンなものでも浮き彫り模様のあるも
のでもいいが、たっぷりとバターを塗ってからファルスを詰めて低めの温度で
火を通す[^0202_10]。

プレーンな型を使う場合、きれいに切ったトリュフのスライスや[ポシェ\*](#pocher-gls)した
[^0202_11]卵の白身、[ロングエカルラット\*](#langue-ecarlate-gls)、ピスタ
チオなどで表面を装飾するといい。

浮き彫り模様の型を使うなら、上記のような装飾は省いていい。

このようなファルスで作った縁飾りを使うのはとりわけ、鶏肉料理、魚料理、牛や羊肉などの料理を盛り込み、ソースをかける場合。

[^0202_9]: moule à bordure（ムーラボルデュール）、ボルデュール型ともいう。
    大きなリング型で、表面に山形の刻み目（浮き彫り模様）の入ったタイプ
    （moule historié ムールイストリエ、またはmoule cannelé ムールカヌ
    レ）と、特に模様の入っていないプレーンなもの(moule uni ムールユニ)
    の2種に大別される。

[^0202_10]: 原文[pocher（ポシェ）\*](#pocher-gls)。ここまでにも何度も出てきた表現だが、茹で
    る場合は「沸騰しない程度の温度で加熱すること」であり、このように型
    に詰めた場合には湯をはった天板に型をのせてやや低温のオーブンに入れ
    てゆっくり加熱することになる。

[^0202_11]: 原文 oeuf poché をそのまま訳したが、表面に飾りとして用いるのは
    固茹で卵の白身をスライスして型抜きあるいはナイフできれいに切ったも
    のを使うことが多い。

[^0202_12]: 本文に指定はないが、原則としては[ファルス A](#farce-a)か、[盛り
    付けの縁飾りおよび底に敷いたり、詰め物をしたクネルに用いる仔牛のファ
    ルス](#farce-de-veau-pour-bordures-de-dressage-fonds-quenelles-fourrees-etc)を用いることになるだろう。






\atoaki{}

#### 野菜で作る縁飾り {#bordures-en-legumes}

<div class="frsubenv">Bordures en Légumes</div>


\index{bordure@bordure!legumes@--- en légumes}
\index{legume@légume!bordures@bordures en ---s}
\index{やさい@野菜!ふちかざり@---で作る縁飾り}
\index{ほるてゆる@ボルデュール ⇒ 縁飾り!やさい@野菜で作る縁飾り}
\index{ふちかさり@縁飾り!やさい@野菜で作る---}


プレーンなボルデュール型の内側にたっぷりとバターを塗り、下拵えしたさま
ざまな野菜を型の底面と側面にシャルトルーズ[^0202_14]状に貼り付けるよう
に敷き詰める。やや固めにじゃがいもで[リエ\*](#lier-gls)した仔牛のファ
ルスを型にいっぱいに詰める（[縁飾り用仔牛のファルス](#farce-de-veau-pour-bordures-de-dressage-fonds-quenelles-fourrees-etc)
参照）。低めのオーブンで湯煎焼きして火を通す。

この縁飾りはもっぱら、牛、羊肉の料理で野菜のガルニチュールをともなうも
のに使う。




[^0202_14]: chartreuse 本文にあるように、野菜を装飾に用いた仕立てのひとつ。
    <!--シャルトル会修道院で作られている同名のスピリッツがあるが、料理にお
    けるシャルトルーズ仕立てもシャルトル会修道院に由来しているという。-->
    シャルトル会は大斉、小斉の決まりに厳格で、野菜を多く食べる修道生活
    を送っていたことで有名。そのことにちなんだ仕立ての名称と言われてい
    る。この仕立ての文献上の初出は1914年刊ボヴィリエ『調理技術』第2巻
    の「りんごのシャルトルーズ仕立て」と思われる。これは今でいうデザー
    トに位置するもので、りんごをサフランやアンゼリカとともに煮て黄色や
    緑に染め、もとの白い果肉、皮の赤など、それら色合いを組み合わせて美
    しく型の底面と側面に貼り付け、内部をりんごのマーマレード（≒ジャム）
    で満たす、というもの(t.2, pp.149-150)。このボヴィリエのシャルトルー
    ズは「原型」というよりはむしろ「バリエーション」的なものであること
    が、レシピ本文の文面から伺える。そのため、いつごろ成立した仕立てな
    のかは不明だが、いずれにしてもシャルトルーズはカレームが「アントレ
    の女王」と呼んだ程に手の込んだ華やかな仕立てとして19世紀前半には定
    着していた。基本的には、円筒形の型に拍子木に切ってそれぞれ下茹でし
    たにんじん、さやいんげん、ナヴェ、などの野菜をびっしりと貼りつけて崩
    れないようにファルスで塗り固める。その内側に、「ペルドリのシャルト
    ルーズ」の場合は、下茹でしたサヴォイキャベツとペルドリ（ペルドロー
    ≒山うずら、の成鳥）をブレゼしたものを詰め、型の上面（提供するとき
    は底面になる）に蓋をするようにファルスを塗ってから、湯煎にかけてファ
    ルスに火を通して固める。裏返して型から外して供する、というもの。野
    菜の配置、配色が重要で技術のいる仕立て（ヘリンボーンのようなパター
    ンが比較的多かったようだ）。なお、「ペルドローのシャルトルーズ」と
    「ペルドリとサヴォイキャベツのブレゼ」を混同しているケースが日本で
    よく見られるが、シャルトルーズとはあくまでも数種類の野菜とファルス
    で表面を装飾する仕立てを意味しているので注意。






\atoaki{}

#### 白い生地で作る縁飾り[^0202_19] {#bordures-en-pate-blanche}

<div class="frsubenv">Bordures en pâte blanche</div>

\index{bordure@bordure!pate blanche@--- en pâte blanche}
\index{pate blanche@pâte blanche!bordures@bordures en ---s}
\index{きし@生地!しろいふちかざり@白い生地で作る縁飾り}
\index{ほるてゆる@ボルデュール ⇒ 縁飾り!しろいきし@白い生地で作る縁飾り}
\index{ふちかさり@縁飾り!しろいきし@白い生地で作る野菜---}
\index{はと@パート ⇒ 生地!しろいふちかざり@白い生地で作る縁飾り}


片手鍋に水1 dLと塩5 g、ラード[^0202_15]30 gを入れ、火にかけて沸騰させる。ふ
るった小麦粉100 gを加えて、余分な水分をとばし、大理石板の上に広げる。

捏ねながらでんぷん[^0202_16]を練り込んでいく。10回生地を折ってから、生地を休ませる。

生地を厚さ7 mm程度にのす。これを専用の抜き型で抜いて飾りのパーツをつく
る。エチューヴ[^0202_17]に入れて乾燥させる。これを卵白に小麦粉を加えた
糊[^0202_18]で皿の縁に貼り付ける。

[^0202_15]: saindoux （サンドゥー）精製した豚の脂。日本語でいうラード。

[^0202_16]: 原文では fécule （フェキュール）すなわち「でんぷん」としか指示
    がないが、fécule de maïs （フェキュールドマイス）コンスターチがいいだろう。

[^0202_17]: 野菜などを乾燥させるためなどの目的で使用する低温で用いるオーブンの一種。

[^0202_18]: repère （ルペール）ここでは小麦粉を卵白に加えて混ぜた糊のこと。
    通常は銀などの金属製の皿に装飾を貼り付ける際に用いる。この場合は
    事前に皿を熱しておき、手早く装飾のパーツを貼る。現代ではほとんど行
    なわれていない手法。小麦粉と水で作り鍋の蓋に目張りをするための生地
    も同じ用語だが、いずれのケースについても「ルペール」という用語は現
    代の日本の調理現場であまり多用されていない。

[^0202_19]: おなじ縁飾り（ボルデュール）でも、美味しくつくれるものと、食べも
    ので出来てはいるけれども実際には食べないことを前提とした装飾では、
    本書において明らかに扱いが異なる。この「白い生地で作る縁飾り」およ
    び次項「ヌイユ生地で作る縁飾り」は後者にあたるため、さして重きを置
    いた説明になっていない。エスコフィエの合理的な考え方がよく表れているところだろう。





\atoaki{}

#### ヌイユ生地で作る縁飾り {#bordures-en-pate-a-nouille}

<div class="frsubenv">Bordures en pâte à nouille</div>

\index{bordure@bordure!pate nouille@--- en pâte à nouille}
\index{nouille@nouille!bordures@bordures en pâte à ---}
\index{きし@生地!ぬいゆふちかざり@ヌイユ生地で作る縁飾り}
\index{ほるてゆる@ボルデュール ⇒ 縁飾り!ぬいゆきし@ヌイユ生地で作る縁飾り}
\index{ふちかさり@縁飾り!ぬいゆきし@ヌイユ生地で作る野菜---}
\index{はと@パート ⇒ 生地!ぬいゆふちかざり@ヌイユ生地で作る縁飾り}

ごく固めに捏ねた[ヌイユ生地](#nouilles)用いて作る縁飾り。上記のよう
に抜き型で抜いてもいいし、あるいは厚さ6〜7 mmで高さ4〜5 cmの帯状に切っ
てもいい。後者は「エヴィドワール」と呼ばれる専用の小さな抜き型を
用いて模様をつけた帯状の生地を皿の縁にしっかりと貼り付ける。

どちらの方法でも、ヌイユ生地を用いた縁飾りには溶いた卵黄を塗ってから、乾燥させる。






\atoaki{}

#### クルトン {#croutons}

<div class="frsubenv">Croûtons</div>


\index{croutons@croûtons}
\index{くるとん@クルトン}

クルトンはいわゆる食パン[^0202_20]で作る。形状や大きさは、どんな料理に合わ
せるかで決まってくる。これを澄ましバター[^0202_21]で揚げるが、揚げるのは必ず提供
直前にすること。

[^0202_20]: フランス語でpain（パン）とだけ言う場合はバゲットに代表されるリー
    ンなパンを指すのが普通で、イギリス式およびアメリカ式の「食パン」は
    pain de mie（パンドミ）と呼ばれて区別される。

[^0202_21]: バターには少なからずカゼインなどの不純物が含まれており、それら
    が焦げや色むらの原因となるので、充分よく澄んだバターを使うこと。






\atoaki{}

#### デュクセル[^0202_22]・セッシュ<!--[^0202_23]--> {#duxelles-seche}

<div class="frsubenv">Duxelles sèche</div>


\index{champignon@champignon!duxelles seche@duxelles sèche}
\index{duxelles@duxelles!seche@--- sèche}
\index{てゆくせる@デュクセル!せつしゆ@---・セッシュ}
\index{まつしゆるむ@マッシュルーム!てゆくせる@デュクセル!せつしゆ@デュクセル・セッシュ}

デュクセルはベースとして必ず、[アシェ\*](#hacher-gls)にした
\ruby{茸}{きのこ}を用いるが、食用のものならどんな茸でも構わない。

バター 30 gと植物油30 gを鍋に熱し、玉ねぎのアシェとエシャロットのアシェ
を各大さじ1杯ずつ入れて、軽く炒める。マッシュルームの切りくず
[^0202_23b]と軸を細かくアシェにしたもの250 gを加え、よく圧して水気を出
させる。水分が完全に蒸発するまで強火で炒め続ける。塩こしょうで調味し、
パセリのアシェ1つまみを加えて仕上げる。[陶製の器に移し入れ
\*](#debarrasser-gls)、バターを塗った紙で蓋をする。

デュクセル・セッシュは多くの料理で使われる。

[^0202_22]: 俗説では17世紀にユクセル侯爵<!-- Marquis d'Uxelles（マルキ デュクセ
    ル）-->に料理長として仕えていたラヴァレーヌが創案し、主人の名を付け
    たとされている。<!--d' は de + 母音の短縮形（フランス語文法ではエリジ
    オンという）。貴族の場合は領地名の前に de (≒ of, from) を付ける慣
    習があり、爵位 de 領地名、というのが正式な呼び名として用いられてい
    た。Uxellesは母音で始まるからd'Uxellesとなり、それが料理用語として
    ひとつの単語となりduxellesとして定着したという。-->しかし、
    duxelles（デュクセル）あるいはそれに類似する名称が用いられるように
    なったのは19世紀以降であり、文献によって綴りも安定していない。19世
    紀末のファーヴル『料理および食品衛生事典』では duxel という綴りで
    項目が立てられている（なお、ファーヴルはデュクセルをアパレイユの一
    種と明確に定義している）。さらに時代を遡っていくと、オドの1858年版
    ではDurcelle（デュルセル）またはDuxelleという名称で呼ばれていると
    記述がある(p.167)。1856年刊グフェ『料理の本』ではd'Uxelles (p.72)。
    1833年刊カレーム『19世紀フランス料理』第3巻には、sauce à la
    Duxelle「ソース・デュクセル」が掲載されている。これはあくまでも
    「ソース」ではあるが、ベースとしてマッシュルームの[アシェ\*](#hacher-gls)を使っ
    ている点は他と同様。さらにヴィアール『王国料理の本』1820年版(p.74)
    および1814年刊ボヴィリエ『調理技術』(p.73)には、のちのデュクセル・
    セッシュとほぼ同様のものがDurcelleの綴りで掲載されている。カレーム
    は[マヨネーズ](#mayonnaise)の訳注でも見たとおり、料理名の綴りに独
    自のこだわりを持つ傾向が強かったので、あるいはカレームがdurcelleか
    らduxelleへの転換点として存在している可能性はある。Durcelleの語と
    しての成り立ちは不明だが、人名（名字）に時折見られる綴りのため、何
    かの由来があったことまでは推察される。以上を考慮すると、ユクセル侯
    爵の名を冠したという説がどんなに早くとも19世紀中葉以降のものだとわ
    かる。フランス語の/R/と/k/の音がやや似て聞こえることがあるために、
    はじめdurcelleと呼ばれていたアパレイユがduxelleとなり、ひいては歴
    史上の人物Marquis d'Uxellesユクセル侯爵に結びつけられるようになっ
    た、と考えられよう。<!--とはいえ、17世紀はいわゆるマッシュルームの人工
    栽培が実用化され、食材として流行した時代でもあっため、ラ・ヴァレー
    ヌとユクセル侯爵をこのアパレイユに関連付けたのもまったくの見当違いで
    とは言えまい。-->

[^0202_23b]: マッシュルームを[トゥルネ\*](#tourner-gls)した際に出た切
りくずを使う。詳しくは[魚のフュメ](#fumet-de-poisson)訳注参照。

<!--[^0202_23]: sec / sèche （セック/セッシュ）乾燥した、水気のない、の意。-->






\atoaki{}

#### 野菜の[ファルシ\*](#farci-gls)<!--[^0202_24]-->用デュクセル {#duxelles-pour-legumes-farcis}

<div class="frsubenv">Duxelles pour légumes farcis</div>

\index{champignon@champignon!duxelles legumes farcis@duxelles pour légumes farcis}
\index{duxelles@duxelles!legumes farcis@--- pour légumes farcis}
\index{てゆくせる@デュクセル!やさいのふあるしよう@野菜のファルシ用---}
\index{まつしゆるむ@マッシュルーム!てゆくせる@デュクセル!やさいのふあるしよう@野菜のファルシ用デュクセル}


（トマト、茸などの詰め物用）

[デュクセル・セッシュ](#duxelles-seche)100 g、すなわち大さじ[^0202_25]4杯を
用意する。白ワイン $\frac{1}{2}$ dLを加えてほぼ完全に煮詰める。次に、トマト
を[ソース・ドゥミグラス](#sauce-demi-glace)1 dLと小さめのにんにく1片を
つぶしたもの、パンの身25 gを加える。

ごく弱火にかけて煮込み、詰め物をするのにちょうどいい固さになるまで煮詰める。

<!--[^0202_24]: farci （ファルシ）詰め物をした、の意。-->

[^0202_25]: 本書における「大さじ1杯」の表現は非常にあいまいで、ざっくりとし
    た分量表示であることに注意。







\atoaki{}

#### ガルニチュール用デュクセル {#duxelles-pour-farnitures-diverses}

<div class="frsubenv">Duxelles pour garnitures diverses</div>


\index{champignon@champignon!duxelles pour garnitures diverses@duxelles pour garnitures diverses}
\index{duxelles@duxelles!garnitures diverses@--- pour garnitures diverses}
\index{てゆくせる@デュクセル!かるにちゆるよう@ガルニチュール用---}
\index{まつしゆるむ@マッシュルーム!てゆくせる@デュクセル!かるにちゆるよう@ガルニチュール用デュクセル}

（タルトレット、玉ねぎ[^0202_27]、[コンコンブル\*](#concombre-gls)などの詰め物用）

[デュクセル・セッシュ](#duxelles-seche)100 gに、[ファルス・ムスリー
ヌ](#farce-c)または[パナードを用いたファルス](#farce-a)もしくは[ファル
ス・グラタン](#farce-gratin-a)60 gのいずれかを料理に合わせて加える。

このデュクセルを野菜の詰め物として用いた場合は、表面を焦がさないように
低温のオーブンで[ポシェ\*](#pocher-gls)すること。

<!--[^0202_26]: 20世紀末頃から日本の種苗メーカーが育種した品種も栽培されるよう
    になってきているため、あえて「きゅうり」と訳したが、伝統的な
    concombre （コンコンブル）は太さ4〜5 cm、長さ30〜45 cm程度まで大きく
    するのが一般的で、日本の現代品種と異なり表皮は固く、苦味やアクは少ない。
    種の部分をスプーンなどで取り除いて、そこに詰め物して加熱調理する。
    また、生のまま輪切りにして食べることも多い。-->

[^0202_27]: 玉ねぎには、完熟、乾燥させた際に表皮が黄色いタイプと白いも
    の、赤紫色の3系統がある。黄色系統の玉ねぎはフォンなどに用いられる
    ことが多い（日本ではこのタイプがほとんど。また「泉州黄」という品種
    はフランスの野菜栽培の専門書でも言及がある程に栽培特性とクオリティ
    が高く評価されて、フランスでも栽培されている）。白玉ねぎ（oignon
    blanche オニョンブロン）は生食やその他の調理、小さいものは[ブロン
    シール\*](#blanchir-gls)してからバターで色よく炒めて（グラセ）ガル
    ニチュールに用いられる。火が通りやすく、甘いものが多い。日本のペコ
    ロスが黄玉ねぎの小さなものがほとんどであることに注意。赤紫のものは
    品種によって特性が違うが、加熱調理、生食いずれにも用いられる。

<!--[^0202_28]: 表面に焦げ目を付けることを gratiner （グラティネ）という。

[^0202_29]: pocher （ポシェ）。本来は沸騰しない程度の温度で茹でることを指す
    が、この場合は比較的低温のオーブンで加熱調理するという意味。-->





\atoaki{}

#### デュクセル・ボヌファム[^0202_37] {#duxelles-a-la-bonne-femme}

<div class="frsubenv">Duxelles à la bonne femme</div>


\index{champignon@champignon!duxelles bonne femme@duxelles à la bonne femme}
\index{duxelles@duxelles!bonne femme@--- à la bonne femme}
\index{bonne femme@bonne femme!duxelles@duxelles à la ---}
\index{てゆくせる@デュクセル!ほぬふあむ@---・ボヌファム}
\index{ほぬふあむ@ボヌファム!てゆくせる@デュクセル・---}
\index{まつしゆるむ@マッシュルーム!てゆくせる@デュクセル!ほぬふあむ@デュクセル・ボヌファム}

（家庭料理用）

生のデュクセルに、しっかり味付けをした[ソーセージ用挽肉](#chair-a-saucisse)を同量加えるだけ。


[^0202_37]: bonne femme（ボヌファム）は「おばさん」くらいの意。家庭風、田舎風の素朴さを感じさせる料理に付けられる名称。


<!--20190418江畑校正確認スミ-->


\atoaki{}

#### トマトエッセンス {#essence-de-tomate}

<div class="frsubenv">Essence de tomate</div>

\index{tomate@tomate!essence@essence de ---}
\index{essence@essence!tomate@--- de tomate}
\index{とまと@トマト!えつせんす@---エッセンス}
\index{えつせんす@エッセンス!トマト---}

よく熟したトマトのジュースを\ruby{漉}{こ}し器でこす。これを片手鍋に入れて、弱火に
かけてゆっくりと、シロップ状になるまで煮詰める。

布で漉すが、\ruby{圧}{お}したり絞らないこと。そのまま保管しておく。


###### 【原注】 {#nota-essence-de-tomate}

このトマトエッセンスは[ブラウン系の派生ソース](#petites-sauces-brunes-composees)の仕上げに色合いを調節する
のにとても便利だ。

<!--[^0202_30]: [ブラウン系の派生ソース](#petites-sauces-brunes-composees)の節で、明示的にこのトマトエッセンスの使
    用に言及しているレシピは2つのみだが、必ずしもそのことにこだわらず、適
    宜、必要に応じて使うのがいい。-->





\atoaki{}

#### 料理をのせる台、トンポン、クルスタード {#fonds-de-plats}

<div class="frsubenv">Fonds de plats, Tampons et Croustades</div>

\index{fonds@fonds!plats@--- de plats}
\index{tampon@tampon}
\index{croustade@croustade}
\index{たい@台!さらにしいてりようりをのせる@皿に敷いて料理をのせる---}
\index{とんほん@トンポン}
\index{くるすたと@クルスタード}


皿に敷いて料理をのせる台、トンポン、クルスタードの重要性は日々ますます
失なわれつつある。新しいサーヴィスの方式ではこれらをほぼ完全に用いられて
はいない。これらの装飾的な台はパンや、一番多いケースは米を材料に作られ
る[^0202_34]。

パンを使った台は、固くなったパンの身を切って作る。これをバターで揚げ、
小麦粉を卵白に加えて作った糊[^0202_32]で皿の底に貼り付ける。

###### 米で作るトンポンとクルスタード

……パトナ米2 kgを、水が完全に澄むまでよく洗う。

たっぷりの湯で5分間[ブロンシール\*](#blanchir-gls)する。鍋の湯を捨て、
別の湯に漬けて米を洗う。再度湯をきる。大きな片手鍋に丈夫で清潔な布また
は豚背脂のシートを敷き、みょうばん10 \nolinebreak[4]gを加え、布または
豚背脂のシートを折り畳んで米を包む。鍋に蓋をして、弱火のオーブンかエ
チューヴ[^0202_33]に入れ、3時間加熱する。

その後、米を力をこめてすり潰す。ラードを塗った布のナフキンで包んで揉み、
ラードを塗った器に手早く詰めて、冷ます。

充分に冷めたら、米の塊を彫って装飾する。みょうばんを加えた水に漬けて、
こまめに水を替えてやれば長期保存も可能だ。

<!--[^0202_31]: 澄ましバターを用いること。-->

[^0202_32]: 説明的に訳したが、原文は repère（ルペール）の1語。[白い生地で作る縁飾
    り](#bordures-en-pate-blanche)および訳注参照。

[^0202_33]: étuve 主として野菜の乾燥などを目的とした低温専用のオーブン。

[^0202_34]: 実際、本書においてこれらを用いる指示は非常に少ないが、まったく
    ないわけでもない。ただ、エスコフィエが乗り越えたいと願ったデュボワ
    とベルナールの『キュイジーヌ・クラシック』がこれらの装飾的な台の作り方にかなりのペー
    ジを割いていることと比較すると、驚くほどに素気なく短かい説明で終わっ
    ている。







\atoaki{}

####  ポルチュゲーズ[^0202_36]、トマトフォンデュ {#fondue-de-tomate-ou-portugaise}

<div class="frsubenv">Fondue de tomate ou Portugaise</div>

\index{tomate@tomate!fondue@fondue de --- ou Portugaise}
\index{portugais@portugais(e)!fondue de tomate@foudue de tomate ou Portugaise}
\index{とまと@トマト!ふおんてゆ@---のフォンデュ、ポルチュゲーズ}
\index{ふおんてゆ@フォンデュ!とまと@トマトの---、ポルチュゲーズ}
\index{ほるちゆけす@ポルチュゲーズ/トマトのフォンデュ}
\index{ほるとかるふう@ポルトガル風!ほるちゆけす@ポルチュゲーズ、トマトのフォンデュ}



玉ねぎ大 1 個を[アシェ\*](#hacher-gls)にしてバターまたは植物油で炒める。トマト 500 gは
皮を剥いて潰し、[コンカセ\*](#concasser-gls)にして鍋に加える。潰したにんにく 1片 と塩、
こしょうを加える。弱火にかけて水分がすっかりなくなるまで煮詰める[^0202_35]。

時季、つまりトマトの熟し具合に応じて必要なら粉砂糖をほんの1つまみ加え
るといい。

[^0202_35]: トマトは品種にもよるが、混ぜずに弱火で加熱すると固形物が沈殿し、
    水分が上澄みになる。ここでは濃縮トマトペーストになるほどは煮詰めず、
    その上澄みがなくなるまで、という解釈でいいだろう。


[^0202_36]: portugais(e) （ポルチュゲ/ポルチュゲーズ）は形容詞の場合は
    「ポルトガルの」の意。名詞の場合はポルトガル人。ここでは大文字で書
    き出していることから名詞と考えられる（なお現代フランス語の正書法で
    は文頭以外の語は固有名詞のみ大文字で始めることになっており、普通名
    詞を文中で大文字にすることはないが、料理名などの場合は比較的自由に
    大文字を使う傾向にある）。すなわち「ポルトガルの女」くらいの意味に
    とることが可能。ちなみに、このレシピとはまったく関係ないが、、
    *Lettres Portugaises* （レットルポルチュゲーズ）『ぽるとがる
    文（ぶみ）』という題名の本が17世紀にフランスで出版され人々の
    感動を誘った。リルケや佐藤春夫が自国語に翻訳、翻案したものも有名。
    実在したポルトガルの修道女マリアナ・アルコフォラドがフランス軍人に
    宛てた恋文をまとめた、事実にもとづく書簡集と考えられていたが、20世
    紀になってから、ガブリエル・ド・ギユラーグという男性文筆家によるまっ
    たくの創作であることが証明された。いわゆる「書簡体小説」である。と
    はいえ作品の文学的価値はまったく減じることのない名作。書簡体小説と
    いう形式は18世紀に流行し、ゲーテ『若きウェルテルの悩み』やラクロ
    『危険な関係』、ルソー『新エロイーズ』などの名作がある。19世紀前半
    にはその流行も落ち着き、バルザック『二人の若妻の手記』などはこの小
    説形式の流行の最後を飾る名作のひとつとして名高い。なお、トマトは16
    世紀に既にフランスにもたらされており、16世紀末に出版されたオリヴィ
    エ・ド・セール『農業経営論』では「美しいが食べても美味しくない」と
    記されている。食材として広く普及したのは19世紀以降であり、爆発的な
    流行現象とさえいえるほどだった。第二帝政期を代表する小説家のひとり
    フロベールの遺作『ブヴァールとペキュシェ』にも農業に挑戦した2人の
    主人公がトマトの芽掻きをする必要があることを知らなかったために失敗
    したエピソードが描かれている。[オマール・アメリケー
    ヌ](#homard-a-l-americaine-a)や、[ソール・デュグレ
    レ](#sole-duglere)などトマトが重要な役割を果している料理が多く創案
    され、フランス料理の歴史において19世紀という時代を象徴する食材のひ
    とつともいえる。










\atoaki{}

#### ポタージュ用そば粉のカーシャ[^0202_42] {#kache-de-sarrazin-pour-potages}

<div class="frsubenv">Kache de Sarrazin pour Potages</div>

\index{kache@kache!sarrazin@--- de Sarrazin pour Potages}
\index{sarrazin@sarrazin!kache@Kache de --- pour Potages}
\index{かしや@カーシャ!そはこ@ポタージュ用そば粉の---}
\index{そはこ@そば粉!かしや@ポタージュ用---のカーシャ}


（仕上がり約10人分[^0202_38]）

粗挽きのそば粉1 kgに塩を加えたゆるま湯を7〜8 dL加えてデトランプ[^0202_39]を
作ってまとめる。これを深手の片手鍋[^0202_40]に入れて押し潰す。高温のオーブ
ンに入れて約2時間加熱する。

オーブンから出したら、表面の固くなった皮の部分は取り除く。鍋の中のパン
状になったものを、鍋の周囲にこびりついた焦げの部分に触れないようにして
取り出す。

これにバター100 gを加えて捏ねる。厚さ1 cmになるように重しをして冷ます。
直径26〜27 mm位の[^0202_41]の丸い型抜きで抜く。これを澄ましバターで色よく
焼く。オードブル皿か、ナフキンに盛り付けて供する。

###### 【原注】 {#nota-kache-de-sarrazin-pour-potage}

このカーシャをオーブンから出してそのままの状態で供してもいい。その場合は専用の容器に盛りつける。

[^0202_42]: フランス語では kache （カシュ）、Kacha （カシャ）とも。日本語で
    はカーシャと呼ばれるほうが多いようだ。もとはロシアなどスラブ諸国に
    おける粥の総称。フランス料理に取り入れられ、そば粉やセモリナ粉でつくったクレー
    プのようなものを意味するようになった。このカーシャはポタージュのガ
    ルニチュール、つまり「浮き実」となる。

[^0202_38]: 原文 pour un service （プランセルヴィス） フランス宮廷料理の時
    代から、ロシア式サービスの普及しはじめた頃まで、格式ある宴席での料理を作
    る際の単位としてserviceが用いられた。1 service は概ね10人分。現実
    には8〜12人くらいの間で融通を効かせて運用されていたようだ。本書の
    レシピの分量は多くが1 serviceすなわち約10人分で書かれている。

[^0202_39]: ここでは動詞 détremper （デトロンペ）が使われているが、faire un
    détrempe （フェランデトロンプ）と同義で粉が吸水して捏ねる前の状態（塊）のこと。

[^0202_40]: casserole russe （カスロールリュス）直訳すると「ロシアの片手鍋」だが、通常は深い片手鍋をそう呼ぶ。

[^0202_41]: 原文 un emporte-pièce rond de la grandeur d'une pièce de 2
    francs 「2フラン硬貨の大きさの円形の抜き型」。フランはヨーロッパ通
    貨統合前のフランスの通貨単位。2フラン硬貨は概ね26〜27 mm。







\atoaki{}

#### クリビヤック[^0202_44]用セモリナ粉のカーシャ {#kache-de-semoule-pour-coulibiac}

<div class="frsubenv">Kache de Semoule pour le Coulibiac</div>

\index{kache@kache!semoule@--- de Semoule pour le Coulibiac}
\index{semoule@semoule!kache@Kache de --- pour le Coulibiac}
\index{かしや@カーシャ!せもりなこ@クリビヤック用セモリナ粉の---}
\index{せもりなこ@セモリナ粉!かしや@クリビヤック用セモリナ粉---のカーシャ}
\index{くりひやつく@クリビヤック!かしや@---用セモリナ粉のカーシャ}

（仕上がり約10人分）

大粒のセモリナ粉200 gに溶き卵1個をよく混ぜる。天板の上に広げて弱火で乾燥させる。

これを目の粗い漉し器で裏漉しする。コンソメに入れて約20分間[ポシェ\*](#pocher-gls)する<!--[^0202_43]-->。気をつけて水気をきる。

<!--[^0202_43]: pocher （ポシェ）。-->

[^0202_44]: サーモンなどをブリオシュ生地で包んで焼いた料理。これを作る際に、
    厚さ1 cmくらいに切ったサーモンの身とこのカーシャまたは米を互いに層に
    なるようにして、ブリオシュ生地で包んで焼く。








\atoaki{}

#### マティニョン[^0202_45] {#matignon}

<div class="frsubenv">Matignon</div>


\index{matignon@matignon}
\index{まていによん@マティニョン}

にんじん125 g、玉ねぎ125 g、セロリ50 g、生ハム100 gを1 cm弱のさいの目
に刻む。ローリエの葉1枚とタイム1枝とともに鍋に入れて、バターで弱火にか
け蓋をして[エチュヴェ\*](#etuver-gls)し、少量の白ワインで[デグラセ\*](#deglacer-gls)する。

[^0202_45]: 1935年以来首相官邸として使われているマティニョン館を18世紀に所
    有していたジャック・ド・マティニョンの料理人が創案したものといわれ
    ているが真偽は不明。料理用語としての初出はおそらく1856年刊デュボワ、
    ベルナール共著『キュイジーヌ・クラシック』。ここでは「マティニョンのフォン」として、
    「器具で[ラペ\*](#raper-gls)した豚背脂、同量のバター、生ハムのスライス数枚、
    [エマンセ\*](#emincer-gls)にしたにんじんと玉ねぎ、マッシュリュームの切りくずを弱火にか
    けて軽く色付くまで炒め、ローリエの葉、塩、こしょうを加えてからマデ
    イラ酒かソテルヌのワインをひたひたに注ぎ、強火でグラス状になるまで
    煮詰める。これを串焼きあるいはオーブン焼きにする塊肉を覆うのに使う。
    魚料理の場合には豚背脂とハムをバターか植物油に代える(p.71)」となっ
    ている。これ以前の主要な料理書にmatignonの語はまったく見られないが、
    1858年版のオドにおいて「ミルポワとマティニョンは野菜と豚背脂、ハム
    を煮てグラス状に煮詰めたガルニチュール。鶏やジビエを串刺しでロース
    トする際にこれで覆ってさらにバターを塗った紙で包む。高級料理でしか
    ほとんど用いられない(p.167)」とされている。








\atoaki{}

#### ミルポワ[^0202_48] {#mirepoix}

<div class="frsubenv">Mirepoix</div>


\index{mirepoix@mirepoix}
\index{みるほわ@ミルポワ}

材料は[マティニョン](#matignon)とまったく同じだが、[ブリュノワーズ
\*](#brunoise-gls)に刻むことと、ハムではなく塩漬け豚バラ肉の脂身の少な
いところをさいの目に切って[ブロンシール\*](#blanchir-gls)したものを使
う場合もある。

バターで[ルヴニール\*](#revenir-gls)する。


[^0202_48]: 18世紀にガストン・ピエール・レヴィ・ミルポワ公爵（1699〜
    1757）の料理人が考案したといわれているが真偽は不明。料理書における
    初出はおそらく1814年刊ボヴィリエ『調理技術』(p.61)だが、非常に厄介
    な問題を含んでいる。というのも、まずpoêle（ポワル）という名称のソー
    スがあり（これがのちのà la poêle > poêlé という調理の歴史につなが
    る）、それは、[デ\*](#des-gls)に切った仔牛腿肉2 kgとハム750 g、器
    具を使って[ラペ\*](raper-gls)するかデに刻んだ豚背脂750 g、さ
    いの目に切ったにんじん5〜6本、玉ねぎ8個は切らずにそのまま、ブーケ
    ガルニとしてパセリ、シブール（≒葱）、クローブ、ローリエの葉2枚、
    タイム、バジル少々と外皮を剥いて種を取り除いたレモンのスライスを、
    500 gのバターで弱火で炒め、ブイヨンかコンソメを注ぎ、4〜5時間アク
    を引きながら煮込み、漉す、というもの。そして、ミルポワとはこのポワ
    ルにブイヨンの$\frac{1}{4}$量をシャンパーニュか上等の白ワインにし
    て作ったもの、となっている。1833年のカレーム『19世紀フランス料理』
    第1巻におけるミルポワも同工異曲であり、さいの目に切った材料をブイ
    ヨンで煮込んで布で絞り漉したもの。さて、1856年のデュボワ、ベルナー
    ル共著『キュイジーヌ・クラシック』においては「ミルポワのフォン」と
    してソースのベースとして掲載されている。概要は、器具を用いてラペす
    るか細かく[アシェ\*](#hacher-gls)した豚背脂300 gを鍋に入れて溶かし、
    玉ねぎ1個とにんじん1本の[エマンセ\*](#emincer-gls)を加え、弱火でゆっ
    くり色付かないよう炒める。さらに大きめのさいの目に切ったハム250 g
    とブーケガルニ、パセリ、マッシュルームの切りくず、にんにく、クロー
    ブを加えて2 Lのブイヨンと $\frac{1}{2}$ Lの白ワインを注ぐ。強火に
    かけ沸騰したら端に寄せて弱火にし、沸騰状態を保ったまま
    $\frac{2}{3}$量まで煮詰める。最後に漉し器で漉す、というもの
    (pp.70-71)。1867年のグフェ『料理の本』においても「ミルポワすなわち
    肉と野菜のエッセンス」となっている(p.406)。つまり、デュボワとベル
    ナールあるいはグフェの頃、つまり19世紀後半まで、ミルポワとは「出汁」
    の一種あるいは液体調味料のようなものだったと考えていい。前項のマティ
    ニョンの訳注でも見たように、1858年版のオドでやや違った認識がされて
    いることは注目に値しよう。19世紀末のファーヴルの『料理および食品衛
    生事典』ではアパレイユとして定義している。さいの目に刻んだハム、に
    んじん、玉ねぎを白こしょう、タイム、バジル、ローリエの葉、クローブ
    とともに色付くまで炒め、ソースやブレゼの調理に用いる、とある。おそ
    らくはファーヴルの示したミルポワがもっとも本書のものに近いが、ファー
    ヴルはマティニョンに言及していないため、さいの目に刻む大きさによっ
    て呼び名を変えているのは本書が文献上最初のものと思われる。なお、
    ファーヴルはミルポワ公爵の料理人が創案したという説をとっている。い
    ずれにしてもミルポワという言葉の指す内容、用途が19世紀後半の30年く
    らいの間に大きく変化したと考えていいだろう。なお、現代日本の調理現
    場ではミルポワとマティニョンを厳密に区別することなく、また、豚背脂
    やハムは用いず、にんじんや玉ねぎなどの香味野菜を細かいさいの目に刻
    んだものをミルポワの用語で統一しているケースも多いようだ。




\atoaki{}

#### ボルドー風ミルポワ {#mirepoix-fine-dite-a-la-bordelaise}

<div class="frsubenv">Mirepoix fine, dite à la Bordelaise</div>

\index{mirepoix@mirepoix!fine@ --- fine, dite à la Bordelaise}
\index{bordelais@bordelais(e)!mirepoix fine@mirepoix fine, dite à la ---e}
\index{みるほわ@ミルポワ!ほるとふう@ボルドー風ミルポワ}
\index{ほるとふう@ボルドー風!みるほわ@---ミルポワ}

標準的な大きさに刻んだ[ミルポワ](#mirepoix)を料理に加えると、普通は即座にその料理に
ふさわしい香り付けが出来るが、ボルドー風ミルポワはとりわけ[エクルヴィス\*](#ecrevisses-gls)
やオマールの料理の風味付けにいい。これはあらかじめ用意してお
くべきもので、次のように作業する。

にんじん125 gと玉ねぎ125 g、パセリ1枝を出来るだけ細かい[ブリュノワーズ\*](#brunoise-gls)に刻む。これにタイム1つまみと粉末にしたローリエの葉1つまみを加える。

材料をバター50 gとともに片手鍋に入れ、完全に火が通るまで蓋をして弱火で[エチュヴェ\*](#etuver-gls)する<!--[^0202_54]-->。

小さな陶製の[器に広げ\*](#debarrasser-gls)、フォークの背を使って器に押し込む。バターを塗った
白い円形の紙で蓋をして、使用するまで保存する。


###### 【原注】 {#nota-mirepoix-fine}

より細かいミルポワを作るには、材料を[アシェ\*](#hacher-gls)にして、トー
ション[^0202_55]の端で材料を強く圧して野菜の水気を出してしまうだけでい
い。こうすると[エチュヴェ\*](#etuver-gls)している間に蒸発しきれない水
分だけが残る。ただし、こうしてミルポワに残った水分は、長い時間保存する場
合にはカビや腐敗の原因になるので注意すること。





<!--[^0202_54]: étuver （エチュヴェ）。-->

[^0202_55]: [ソース・ヴェルト](#sauce-verte)訳注参照。








\atoaki{}

#### 丸鶏の詰め物その他に用いる精白大麦[^0202_57] {#orge-perle-pour-volailles-farcies}

<div class="frsubenv">Orge perlé pour volailles farcies et autres usages</div>

\index{orge perle@orge perlé}
\index{しんしゆむき@真珠麦!まるとりのつめもの@丸鶏の詰め物その他に用いる真珠麦}
\index{おおむき@大麦!せいはく@精白--- ⇒ 丸鶏の詰め物その他に用いる真珠麦}
\index{まるむぎ@丸麦 ⇒ 真珠麦!まるとりのつめもの@丸鶏の詰め物その他に用いる真珠麦}

玉ねぎの[アシェ\*](#hacher-gls)75 gをバターでブロンド色になるまで炒め
る。皮を剥いて洗い、水気をきってさらに布で水気を取り除いた大麦250 gを
加える。木のヘラで混ぜながら炒める。沸かした[白いブイヨ
ン](#consomme-blanc-simpie)[^0202_56]$\frac{3}{4}$ Lを注ぐ。こしょう1
つまみを加えたら蓋をしてごく弱火のオーブンで約2時間加熱する。焦がしバ
ター50 gをかけて仕上げる。

[^0202_56]: 原文どおりに訳したが、第四版に bouillon blanc は掲載されていな
    い。ここでは「[白いコンソメ」すなわちコンソメ・サンプル](#consomme-blanc-simple)と解釈するの
    がいいだろう。

[^0202_57]: このレシピは第四版のみ。






\atoaki{}

#### 調理用シュー生地 {#pate-a-chou-d-office}

<div class="frsubenv">Pâte à chou d'office</div>

\index{pate@pâte!chou@chou!office@pâte à chou d'office}
\index{choupate@chou (pâte)!office@pâte à --- d'office}
\index{しゆきし@シュー生地!ちようりよう@調理用---}
\index{きし@生地!ちようりようしゆ@調理用シュー生地}


水1 Lとバター200 g、塩 10 gを片手鍋に入れて火にかけ、沸騰したら火から外
す。\ruby{篩}{ふる}った小麦粉625 gを加える。強火にかけて混ぜながら余計な水分をと
ばす。次に、卵の大きさによって12〜14個の全卵を生地に加える。

このシュー生地は[じゃがいものドフィーヌ](#pommes-de-terre-dauphine)やニョッキな
どのアパレイユとして使用されるのがほとんどなので、[通常のシュー生地](#pate-a-chou-ordinaire)より
も固く作らなくてはいけない。







\atoaki{}

#### 脳、白子のベニェやフリトー[^0202_58]用の揚げ衣 {#pate-a-frire-pour-beignets-de-cervelles-et-de-laitances-fritots-etc}

<div class="frsubenv">Pâte à frire pour Beignets de cervelles et de laitances, fritots, etc.</div>

\index{pate@pâte!frire@--- à frire!beignet cervelles@pâte à frire pour Beignets de cervelles et de laitances, fritots, etc.}
\index{frire@frire!pate@pâte à ---!beignet cervelles@pâte à frire pour Beignet de cervelles et de laitances, fritots; etc.}
\index{fritot@fritot!pate frire@pâte à frire pour Beignet de cervelles et de laitances, fritots; etc.}
\index{cervelle@cervelle!pate frire@pâte à frire pour Beignet de cervelles et de laitances, fritots; etc.}
\index{laitance@laitance!pate frire@pâte à frire pour Beignet de cervelles et de laitances, fritots; etc.}
\index{あけころも@揚げ衣!せるうえるしらこのへにえやふりと@セルヴェル、白子のベニェやフリトー用の揚げ衣}
\index{ふりと@フリトー!せるうえるしらこのへにえやふりとようあけころも@セルヴェル、白子のベニェやフリトー用の揚げ衣}
\index{せるうえる@セルヴェル!しらこのへにえやふりとようあけころも@セルヴェル、白子のベニェやフリトー用の揚げ衣}
\index{しらこ@白子!せるうえるのへにえやふりとようあけころも@セルヴェル、白子のベニェやフリトー用の揚げ衣}

陶製の器に、\ruby{篩}{ふる}った小麦粉125 g、塩1つまみ、植物油か溶かしバター大さじ
2杯、\ruby{微温湯}{ぬるまゆ}2 dLを入れる。木のヘラで生地を持ち上げながら混ぜる。すぐに
使う場合は決して生地を捏ねまわさないこと。弾力が出てしまい、揚げる具材
を漬けたときに生地が上手く付かなくなってしまうからだ。事前に用意してお
く場合には、捏ねまわしても大丈夫。生地を休ませている間に弾力性は失なわ
れる[^0202_59]。

この生地は、使う直前に、ふんわりと泡立てた卵白2個分を加える。

[^0202_58]: かえるの腿、牡蠣、ムール貝、サーモン、鶏のレバーなどをマリネし
    て揚げ衣を付けて油で揚げた料理。friteau （フリトー）とも綴る。とく
    にfrite(s)（フリット）と混同しないように注意したい。名詞としての
    fritesはフライドポテトのこと。過去分詞（形容詞）としての
    frit(e)（フリ/フリット）は「油で揚げた」の意。例えばcourgette
    frite（クルジェットフリット）は油で揚げたズッキーニのこと（friteは
    形容詞）だが、steak frites（ステックフリット）フライドポテト添えの
    ステーキを意味する（この場合のfritesは名詞）。

[^0202_59]: いったん形成されたグルテンはそうそう崩れないので、内容としては
    やや疑問に思う部分だが、日本の「てんぷら」の常識をここで適用すべき
    ではない。実際のところ、フリトーの衣はグルテンが形成されていてもまっ
    たく問題ないだろう。







\atoaki{}

#### 野菜用の揚げ衣 {#pate-a-frire-pour-legumes}

<div class="frsubenv">Pâte à frire pour Légumes</div>

\index{pate@pâte!frire@--- à frire!legumes@pâte à frire pour Légumes}
\index{frire@frire!pate legumes@pâte à frire pour Légumes}
\index{legumes@legumes!pate frire@pâte à frire pour Légumes}
\index{salsifis@salsifis!pate frire@pâte à frire pour Légumes}
\index{celeris@céleris!pate frire@pâte à frire pour Légumes}
\index{crosnes@crosnes!pate frire@pâte à frire pour Légumes}
\index{あけころも@揚げ衣!やさいよう@野菜用の揚げ衣}
\index{さすしふい@サルシフィ!やさいようのあけころも@野菜用の揚げ衣}
\index{せろり@セロリ!やさいようのあけころも@野菜用の揚げ衣}
\index{ちよろき@チョロギ!やさいようのあけころも@野菜用の揚げ衣}

（サルシフィ[^0202_60]、セロリ、クローヌ[^0202_61]など）

陶製の器に小麦粉125 gと塩1つまみ、溶かしバター大さじ2杯、全卵1個、水適量を混ぜて薄めの衣をつくる。

出来るだけ、1時間前に用意しておくこと。


[^0202_60]: salsifis キク科の根菜、見た目は牛蒡に似ているが風味や調理特性はまったく異なる。

[^0202_61]: crosne ちょろぎ。シソ科の根菜（正確には塊茎が食用となる）。中国
原産で日本には江戸時代に伝わった。同様に中国からヨーロッパにも伝
わり、フランスでは最初に栽培された地名からcrosne、あるいは日本由来のも
のとしてcrosne du Japon（クローヌデュジャポン）と呼ばれる。絵画などの
分野でジャポニスムが流行したこともあって、日本の食材として注目を浴びた
ためか、(à la) japonaise （アラ ジャポネーズ）「日本風」を冠したものに
は、このちょろぎを用いた料理が多い。









\atoaki{}

#### 大皿仕立ての丸鶏に詰める米 {#riz-pour-farcir-les-volailles-servies-en-releve-ou-en-entree}

<div class="frsubenv">Riz pour farcir les volailles servies en Relevé ou en Entrée</div>

\index{riz@riz!farcir@--- pour farcir les volailles servies en Relevé ou en Entrée}
\index{farce@farce!riz@riz pour farcir les volailles servies en Relevé ou en Entrée}
\index{ふあるす@ファルス!おおさらしたてのまるとりにつめるこめ@大皿仕立ての丸鶏に詰める米}
\index{こめ@米!つめもの@大皿仕立ての丸鶏に詰める米}




玉ねぎ $\frac{1}{2}$個の[アシェ\*](#hacher-gls)をバター50 gでさっと炒める。カロライナ米
またはパトナ米250 gを加え、米が白くなるまで混ぜながら炒める。

[白いコンソメ](#consomme-blanc-simple) $\frac{1}{2}$ Lを注ぎ、蓋をして15分間煮る。
生クリーム1 $\frac{1}{2}$ dLとフォワグラの脂[^0202_62]またはバター125 g、[ソース・
シュプレーム](#sauce-supreme)大さじ数杯と、この米を詰める鶏料理に添え
ることになっているガルニチュールの一部を加える。


###### 【原注】 {#nota-riz-pour-farcir-les-volailles-servies-en-releve-ou-en-entree}

米は鶏を焼いている間に完全に火が通るよう、詰め物をする段階で
は$\frac{3}{4}$程度に火が通っているようにする。鶏に詰めた米は膨ら
むので、きっちりとは鶏に詰め込まないこと。


[^0202_62]: フォワグラのテリーヌなどを作る際に余分な脂が出るのでそれを利用
    するといい。









\atoaki{}

#### サルピコン[^0202_63] {#salpicons-divers}

<div class="frsubenv">Salpicons divers</div>

\index{salpicon@salpicon}
\index{さるひこん@サルピコン}


サルピコンという用語は普通、ある調理の種類を指すものと理解されよう。

サルピコンにはサンプルとコンポゼ[^0202_65]がある。

素材が1種類だけの場合はサンプルと呼ぶ。例えば鶏やジビエの肉、羊や牛の
肉、仔牛胸腺肉[^0202_66]、あるいはフォワグラ、魚、甲殻類、ハム、舌肉など。

素材が複数からなる場合はコンポゼと呼ぶ。本書に掲載されている組み合わせ
のほか、相性のよさそうなものの組み合わせ、マッシュルームやトリュフで\ruby{嵩}{かさ}
を増したもの、などがそうだ。

サルピコンの作り方は、各種の素材を、小さな規則正しいサイズ、すなわち一
辺が0.5 cm程度のさいの目に刻む。

各種サルピコンのレシピ集を作るとしたら[^0202_74]、上記のような素材の組
み合わせから始まり、それによって使い途も名称も決まることになる。たとえ
ば[^0202_68]、\kenten{ロワイヤル}、\kenten{フィノンスィエール}、
\kenten{パリ風}、\kenten{モングラ}、\kenten{シャスール}など。


[^0202_63]: この項は第二版で全面的に書き換えられ、分量も大幅に増えた。初版
    の記述は以下のとおり。「この用語は一般的に火を通した肉、フアルス、
    マッシュルーム、トリュフなどをさいの目切りにしたもののこと。大きさ
    は合わせる料理に応じて加減する。たった1種類の肉、あるいは野菜のさ
    いの目切りにしたものでもサルピコンと呼ぶ。（例）フォワグラのサルピ
    コン、トリュフのサルピコン、など(p.188)」。

[^0202_74]: 原文は直説法現在という時制で書かれており「事実を述べる」ニュア
    ンスだが、本書にサルピコンのレシピをまとめた章も節もないため、やや
    仮定法的に訳した。なお『ラルース・ガストロノミック』初版には
    salpiconの項に代表的なレシピがまとめられている。

[^0202_65]: simple （サンプル）単一の、シンプルな。composé(e) （コンポゼ）組み合わせた。

[^0202_66]: ris de veau （リドヴォ）仔牛胸腺肉。

[^0202_68]: ここに挙げられている例をもうすこし見ていくと、1° ロワイヤ
    ルroyale 王宮風、王家風、の意で、これほど料理そのものと関連なく料
    理名に濫用されている語も珍しいとさえ言えるが、サルピコン・ロワイヤ
    ル salpicon à la royaleの場合は『ラルース・ガストロノミック』初版に
    よると「トリュフとマッシュルームを鶏のピュレであえたもの」を指す。そのほか
    2° フィノンスィーエル salpicon à la financière……[ガルニチュール・
    フィノンスィシエール](#garniture-financiere)の構成素材をさいの目に刻
    み煮詰めたソース・フィナンシエールであえたもの。3° パリ風salpicon
    à la parisienneは[パリ風ガルニチュール](#garniture-parisienne)参照。
    4° モングラsalpicon à la Monglasは本節冒頭の[アパレイユ・モング
    ラ](#appareil-montglas)そのもの。5° シャスールsalpicon
    chasseur……さいの目に切ってバターで炒めた鶏のレバーとマッシュルー
    ムを、煮詰めた[ソース・シャスール](#sauce-chasseur)であえたもの。





\atoaki{}

#### [ピエロギ](#piroguis-caucasiens)用トヴァローグ[^0202_72] {#twarogue-pour-piroguis}


<div class="frsubenv">Twatogue pour Piroguis</div>


\index{piroguis@piroguie!twarogue@twarogue pour ---}
\index{twarogue@twaroguel!piroguis@--- pour piroguis}
\index{cuisine russe@cuisine russe!twarogue@twarogue pour piroguis}
\index{とうあろく@トヴァローグ}
\index{ろしあふう@ロシア風!ひえろきようとうあろく@ピエロギ用トヴァローグ}
\index{ひえろき@ピエロギ!とうあろく@---用トゥヴァローグ}

[^0202_72]: このレシピは第三版から。なお第四版=現行版の綴りはtawrogueになっ
    ているが、明らかに第三版にあるtwarogueの誤植。

よく水気をきったフロマージュ・ブラン[^0202_73]250 gをナフキンでしっかり絞る。
これを陶製の器に入れ、ヘラで滑らかになるまで練る。あらかじめ捏ねてポマー
ド状に柔らかくしておいたバター250 gと全卵1個を加える。

塩、こしょうで調味する。


[^0202_73]: ヨーグルトに見た目のよく似た半固形チーズ。デザートなどとして砂糖をかけて食べるなどが一般的。

</div><!--endRecette-->

<!--20190429江畑校正確認スミ-->
